﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task_mohanu
{
	public class LevelDataModel
    {
		private static readonly string[] Level1 = {
			"000000000000000000",
			"110000000000000000"
		};
		//Hell boss level
		private static readonly string[] Level2 = {
			"000000000000000000",
			"220000000000000000"
		};

		//Purgatory level
		private static readonly string[] Level3 = {
			"000000000000000000",
			"330000000000000000"
		};

		//Purgatory boss level
		private static readonly string[] Level4 = {
			"000000000000000000",
			"440000000000000000"
		};

		//Paradise level
		private static readonly string[] Level5 = {
			"000000000000000000",
			"550000000000000000"
		};

		//Paradise boss level
		private static readonly string[] Level6 = {
			"000000000000000000",
			"660000000000000000"
		};

		//Getter & Setter
public string[] GetLevel1()
	{
	return Level1;
	}

public string[] GetLevel2()
{
	return Level2;
}

public string[] GetLevel3()
{
	return Level3;
}

public string[] GetLevel4()
{
	return Level4;
}

public string[] GetLevel5()
{
	return Level5;
}

public string[] GetLevel6()
{
	return Level6;
}
    }
}
