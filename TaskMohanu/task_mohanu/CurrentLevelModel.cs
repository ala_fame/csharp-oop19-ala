﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task_mohanu
{
	public class CurrentLevelModel
	{
		//Attributes:
		private int currentLevel = ZERO;
		private const int ZERO = 0;

		//Constructor
		/**
		 * Set your current level to 0 (Level 1)
		 * 
		 */

		//Getter & Setter
		public int GetCurrentLevel()
		{
			return currentLevel;
		}

		/**
		 * Set your level to 0
		 */
		public void ForceSetCurrentLevel0()
		{
			currentLevel = ZERO;
		}

		/**
		 * Update your level only if it higher than your current level
		 * 
		 * @param updateLevel
		 */
		public void SetCurrentLevel(int updateLevel)
		{
			if (GetCurrentLevel() <= updateLevel)
			{
				currentLevel = updateLevel;
			}
		}
	}
}
