﻿using System;

namespace task_mohanu
{
    class Test
    {
        

        static void Main(string[] args)
        {
            CurrentLevelModel currentLevel = new CurrentLevelModel();
            LevelDataModel levelData = new LevelDataModel();

            Console.WriteLine(currentLevel.GetCurrentLevel());
            currentLevel.SetCurrentLevel(3);
            Console.WriteLine(currentLevel.GetCurrentLevel());
            currentLevel.ForceSetCurrentLevel0();
            Console.WriteLine(currentLevel.GetCurrentLevel());
            currentLevel.SetCurrentLevel(8);
            Console.WriteLine(currentLevel.GetCurrentLevel());
            currentLevel.SetCurrentLevel(5);
            Console.WriteLine(currentLevel.GetCurrentLevel());
            currentLevel.SetCurrentLevel(10);
            Console.WriteLine(currentLevel.GetCurrentLevel());

            Console.WriteLine(levelData.GetLevel1());
            Console.WriteLine(levelData.GetLevel2());
            Console.WriteLine(levelData.GetLevel3());
            Console.WriteLine(levelData.GetLevel4());
            Console.WriteLine(levelData.GetLevel5());
            Console.WriteLine(levelData.GetLevel6());
        }
    }
}
