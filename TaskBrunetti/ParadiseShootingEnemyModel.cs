﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBrunetti
{
    public class ParadiseShootingEnemyModel : StandardEnemyModel , IParadiseEnemiesAbilitiesModel
    {
        private static readonly double HEALTH = 10;
        private static readonly double DAMAGE_ON_CONTACT = 0;
        private static readonly double R = 0;
        private static readonly double DX = 2;
        private static readonly double DY = 0;
        private static readonly double DR = 0;
        private static readonly double WIDTH = 64;
        private static readonly double HEIGHT = 128;

        private bool firing;

        public ParadiseShootingEnemyModel(double x, double y) : base(x, y, R, DX, DY, DR, HEALTH, DAMAGE_ON_CONTACT, WIDTH, HEIGHT)
        {
            this.firing = false;
        }

        public ParadiseShootingEnemyModel(double x, double y, double r, double dx, double dy, double dr, double health, double damageOnContact) : base(x, y, R, dx, dy, dr, health, damageOnContact, WIDTH, HEIGHT)
        {
            this.firing = false;
        }

        public bool IsFiring()
        {
            return firing;
        }

        public void SetFiring(bool firing)
        {
            this.firing = firing;
        }

        public void MoveEnemy()
        {
            if (this.IsAlive() && !this.firing)
            {
                if (this.GetDx() < 0)
                {
                    this.SetCurrentPosition(this.GetCurrentPosition() - 1);
                    if (this.GetCurrentPosition() < -StandardEnemyModel.GetMaxDistance())
                    {
                        this.SetDx(DX);
                    }
                }
                else if (this.GetDx() > 0)
                {
                    this.SetCurrentPosition(this.GetCurrentPosition() + 1);
                    if (this.GetCurrentPosition() > StandardEnemyModel.GetMaxDistance())
                    {
                        this.SetDx(-DX);
                    }
                }
            }
        }
    }
}
