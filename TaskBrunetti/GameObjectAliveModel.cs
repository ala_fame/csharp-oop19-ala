﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBrunetti
{
    public class GameObjectAliveModel : DynamicGameObjectModel
    {
        private double maxHealth;
        private double health;

        public GameObjectAliveModel() { }

        public GameObjectAliveModel(double x, double y, double r, double width, double height, double dx, double dy, double dr, double maxHealth, double damageOnContact) : base(x, y, r, width, height, dx, dy, dr, damageOnContact)
        {
            this.maxHealth = maxHealth;
            this.health = maxHealth;
        }

        public double GetHealth()
        {
            return health;
        }

        public void SetHealth(double health)
        {
            this.health = health;
        }

        public double GetMaxHealth()
        {
            return maxHealth;
        }

        public void SetMaxHealth(double maxHealth)
        {
            this.maxHealth = maxHealth;
        }

        public bool IsAlive()
        {
            if (this.GetHealth() <= 0)
            {
                return false;
            }
            return true;
        }
    }
}

