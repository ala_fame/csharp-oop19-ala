﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBrunetti
{
    public class DynamicGameObjectModel : GameObjectModel
    {
        private double dx;
        private double dy;
        private double dr;

        public DynamicGameObjectModel() { }

        public DynamicGameObjectModel(double x, double y, double r, double width, double height, double dx, double dy, double dr, double damageOnContact) : base(x, y, r, width, height, damageOnContact)
        {
            this.dx = dx;
            this.dy = dy;
            this.dr = dr;
        }

        public double GetDx()
        {
            return dx;
        }

        public void SetDx(double dx)
        {
            this.dx = dx;
        }

        public double GetDy()
        {
            return dy;
        }

        public void SetDy(double dy)
        {
            this.dy = dy;
        }

        public double GetDr()
        {
            return dr;
        }

        public void SetDr(double dr)
        {
            this.dr = dr;
        }

        public void Move()
        {
            this.SetX(this.GetX() + dx);
            this.SetY(this.GetY() + dy);
            this.SetR(this.GetR() + dr);
        }
    }
}

