﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBrunetti
{
    public abstract class StandardEnemyModel : GameObjectAliveModel
    {
        private static readonly double MAX_DISTANCE = 100;
        private static readonly int STANDARD_DEATH_ANIMATION_COUNTER_VALUE = 0;
        private static readonly int ATTACK_RANGE = 650;

        private int deathAnimationCounter;
        private int currentPosition;
        private bool ranged;

        public StandardEnemyModel(double x, double y, double r, double dx, double dy, double dr, double health, double damageOnContact, double width, double height) : base(x, y, r, width, height, dx, dy, dr, health, damageOnContact)
        {
            this.deathAnimationCounter = STANDARD_DEATH_ANIMATION_COUNTER_VALUE;
            this.currentPosition = 0;
            this.ranged = false;
        }

        public int GetCurrentPosition()
        {
            return currentPosition;
        }

        public static int GetStandardDeathAnimationCounterValue()
        {
            return STANDARD_DEATH_ANIMATION_COUNTER_VALUE;
        }

        public int GetDeathAnimationCounter()
        {
            return deathAnimationCounter;
        }

        public void SetDeathAnimationCounter(int deathAnimationCounter)
        {
            this.deathAnimationCounter = deathAnimationCounter;
        }

        public void SetCurrentPosition(int currentPosition)
        {
            this.currentPosition = currentPosition;
        }

        public static double GetMaxDistance()
        {
            return MAX_DISTANCE;
        }

        public bool IsRanged()
        {
            return ranged;
        }

        public void SetRanged(bool ranged)
        {
            this.ranged = ranged;
        }

        public void CheckRanged(double luciferY)
        {
            if (Math.Abs(luciferY) - Math.Abs(this.GetY()) < StandardEnemyModel.ATTACK_RANGE)
            {
                this.SetRanged(true);
            }
            else
            {
                this.SetRanged(false);
            }
        }

    }
}
