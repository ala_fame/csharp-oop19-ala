﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskBrunetti
{
    public class ParadiseWalkingEnemyModel : StandardEnemyModel, IParadiseEnemiesAbilitiesModel
    {
        private static readonly double HEALTH = 20;
        private static readonly double DAMAGE_ON_CONTACT = 1;
        private static readonly double R = 0;
        private static readonly double DX = 2;
        private static readonly double DY = 0;
        private static readonly double DR = 0;
        private static readonly double WIDTH = 64;
        private static readonly double HEIGHT = 128;

        private bool attacking;

        public ParadiseWalkingEnemyModel(double x, double y) : base(x, y, R, DX, DY, DR, HEALTH, DAMAGE_ON_CONTACT, WIDTH, HEIGHT)
        {
            this.attacking = false;
        }

        public ParadiseWalkingEnemyModel(double x, double y, double dx, double dy, double dr, double health, double damageOnContact) : base(x, y, R, dx, dy, dr, health, damageOnContact, WIDTH, HEIGHT)
        {
            this.attacking = false;
        }

        public bool IsAttacking()
        {
            return attacking;
        }

        public void SetAttacking(bool attacking)
        {
            this.attacking = attacking;
        }

        public void MoveEnemy()
        {
            if (this.IsAlive() && !this.attacking)
            {
                if (this.GetDx() < 0)
                {
                    this.SetCurrentPosition(this.GetCurrentPosition() - 1);
                    if (this.GetCurrentPosition() < -StandardEnemyModel.GetMaxDistance())
                    {
                        this.SetDx(DX);
                    }
                }
                else if (this.GetDx() > 0)
                {
                    this.SetCurrentPosition(this.GetCurrentPosition() + 1);
                    if (this.GetCurrentPosition() > StandardEnemyModel.GetMaxDistance())
                    {
                        this.SetDx(-DX);
                    }
                }
            }
        }
    }
}
