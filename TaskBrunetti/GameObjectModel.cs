﻿namespace TaskBrunetti
{
    public class GameObjectModel
    {
        private double x;
        private double y;
        private double r;

        private double height;
        private double width;

        private double damageOnContact;

        public GameObjectModel() { }

        public GameObjectModel(double x, double y, double r, double width, double height, double damageOnContact)
        {
            this.x = x;
            this.y = y;
            this.r = r;

            this.width = width;
            this.height = height;

            this.damageOnContact = damageOnContact;
        }

        public double GetX()
        {
            return x;
        }

        public void SetX(double x)
        {
            this.x = x;
        }

        public double GetY()
        {
            return y;
        }

        public void SetY(double y)
        {
            this.y = y;
        }

        public double GetR()
        {
            return r;
        }

        public void SetR(double r)
        {
            this.r = r;
        }

        public double GetDamageOnContact()
        {
            return damageOnContact;
        }

        public void SetDamageOnContact(double damageOnContact)
        {
            this.damageOnContact = damageOnContact;
        }

        public double GetHeight()
        {
            return height;
        }

        public void SetHeight(double height)
        {
            this.height = height;
        }

        public double GetWidth()
        {
            return width;
        }

        public void SetWidth(double width)
        {
            this.width = width;
        }
    }
}
