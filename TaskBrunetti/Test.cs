﻿using System;

namespace TaskBrunetti
{
    class Test
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test:");
            Console.WriteLine("Walking Enemy:");
            Console.WriteLine(TestParadiseWalkingEnemyModel());
            Console.WriteLine("Shooting Enemy:");
            Console.WriteLine(TestParadiseShootingEnemyModel());
        }

        private static string TestParadiseWalkingEnemyModel()
        {
            ParadiseWalkingEnemyModel paradiseWalkingEnemyModel = new ParadiseWalkingEnemyModel(0,0);
            for (int i=0;i<=200;i++)
            {
                paradiseWalkingEnemyModel.MoveEnemy();
                paradiseWalkingEnemyModel.Move();
                Console.WriteLine("Posizione: x=" + paradiseWalkingEnemyModel.GetX() + " y=" + paradiseWalkingEnemyModel.GetY());
            }
            
            return "Posizione: x=" + paradiseWalkingEnemyModel.GetX() + " y=" + paradiseWalkingEnemyModel.GetY();
        }

        private static string TestParadiseShootingEnemyModel()
        {
            ParadiseShootingEnemyModel paradiseShootingEnemyModel = new ParadiseShootingEnemyModel(10, 10);
            for (int i = 0; i <= 200; i++)
            {
                paradiseShootingEnemyModel.MoveEnemy();
                paradiseShootingEnemyModel.Move();
                Console.WriteLine("Posizione: x=" + paradiseShootingEnemyModel.GetX() + " y=" + paradiseShootingEnemyModel.GetY());
            }
            return "Posizione: x=" + paradiseShootingEnemyModel.GetX() + " y=" + paradiseShootingEnemyModel.GetY();
        }
    }
}
