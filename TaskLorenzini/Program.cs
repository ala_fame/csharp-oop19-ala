﻿using System;

namespace TaskLorenzini
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Here some info of the objects created using the translated classes:");
            Console.WriteLine(TestDynamicGameObject());
            Console.WriteLine(TestGameObjectAliveModel());
            Console.ReadKey();
        }

        private static string TestDynamicGameObject()
        { 
          DynamicGameObjectModel oggettoProva = new DynamicGameObjectModel(10, 10, 0, 20, 20, 2, 0, 0, 5);
          return "Oggetto Prova di tipo dinamico: x: " + oggettoProva.getX() + " y: " + oggettoProva.getY() + " r: " + oggettoProva.getR() + " width: " + oggettoProva.getWidth() + " height: " + oggettoProva.getHeight() + " dx: " + oggettoProva.getDx() + " dy: " + oggettoProva.getDy() + " dr: " + oggettoProva.getDr() + " damageOnContact: " + oggettoProva.getDamageOnContact();
        }

        private static string TestGameObjectAliveModel()
        {
            GameObjectAliveModel oggettoProva = new GameObjectAliveModel(1050, 700, 0, 40, 80, 0,0,0, 10000, 100);
            return "Oggetto Prova di tipo Vivo: x: " + oggettoProva.getX() + " y: " + oggettoProva.getY() + " r: " + oggettoProva.getR() + " width: " + oggettoProva.getWidth() + " height: " + oggettoProva.getHeight() + " dx: " + oggettoProva.getDx() + " dy: " + oggettoProva.getDy() + " dr: " + oggettoProva.getDr() + " health: " + oggettoProva.getHealth() + " damageOnContact: " + oggettoProva.getDamageOnContact();
        }
    }
}
