﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskLorenzini
{
    class GameObjectAliveModel : DynamicGameObjectModel
    {
        //Attributes:
        private double maxHealth;
        private double health;

        //Constructors:
        public GameObjectAliveModel() { }

        /**
        * Constructor.
        * 
        * @param x
        * @param y
        * @param r
        * @param type
        * @param width
        * @param height
        * @param dx
        * @param dy
        * @param dr
        * @param maxHealth
        * @param damageOnContact
        * 
        */
        public GameObjectAliveModel( double x, double y, double r, double width, double height, double dx, double dy, double dr, double maxHealth, double damageOnContact) : base(x, y, r, width, height, dx, dy, dr, damageOnContact)
        {
            this.maxHealth = maxHealth;
            this.health = maxHealth;
        }

        //Getters&Setters:
        public double getHealth()
        {
            return health;
        }
        public void setHealth( double health)
        {
            this.health = health;
        }
        public double getMaxHealth()
        {
            return maxHealth;
        }

        public void setMaxHealth( double maxHealth)
        {
            this.maxHealth = maxHealth;
        }

        //Methods:
        /**
         * return that the GameObjectAlive is alive or not.
         * @return boolean
         */
        public bool isAlive()
        {
            if (this.getHealth() <= 0)
            {
                return false;
            }
            return true;
        }
    }
}
