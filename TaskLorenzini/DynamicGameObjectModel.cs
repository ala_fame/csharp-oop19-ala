﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskLorenzini
{
    class DynamicGameObjectModel : GameObjectModel
    {
        //Attributes:
        private double dx;
        private double dy;
        private double dr;

        //constructors:
        public DynamicGameObjectModel() { }

        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * @param r
         * @param type
         * @param width
         * @param height
         * @param dx
         * @param dy
         * @param dr
         * @param damageOnContact
         * 
         */
        public DynamicGameObjectModel( double x, double y, double r, double width, double height, double dx, double dy, double dr, double damageOnContact) : base(x, y, r, width, height, damageOnContact)
        {
            this.dx = dx;
            this.dy = dy;
            this.dr = dr;
        }

        //Getters&Setters:
        public double getDx()
        {
            return dx;
        }
        public void setDx( double dx)
        {
            this.dx = dx;
        }
        public double getDy()
        {
            return dy;
        }
        public void setDy( double dy)
        {
            this.dy = dy;
        }
        public double getDr()
        {
            return dr;
        }
        public void setDr( double dr)
        {
            this.dr = dr;
        }

        //Methods:
        /**
         * make DynamicGameObject move.
         * 
         */
        public void move()
        {
            this.setX(this.getX() + dx);
            this.setY(this.getY() + dy);
            this.setR(this.getR() + dr);

            this.moveHitBox(dx, dy);
        }

        /**
         * make hitBox move.
         * 
         * @param dx
         * @param dy
         * 
         */
        public void moveHitBox( double dx, double dy)
        {
            this.getHitBox().setLeftX(this.getHitBox().getLeftX() + dx);
            this.getHitBox().setRightX(this.getHitBox().getRightX() + dx);
            this.getHitBox().setLowY(this.getHitBox().getLowY() + dy);
            this.getHitBox().setHighY(this.getHitBox().getHighY() + dy);
        }

        /**
         * make manual hitBox move.
         * 
         * @param leftX
         * @param rightX
         * @param highY
         * @param lowY
         * 
         */
        public void manualHitBoxMovement( double leftX, double rightX, double highY, double lowY)
        {
            this.getHitBox().setLeftX(leftX);
            this.getHitBox().setRightX(rightX);
            this.getHitBox().setLowY(lowY);
            this.getHitBox().setHighY(highY);
        }
    }
}
