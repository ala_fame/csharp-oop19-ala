﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskLorenzini
{
    public abstract class GameObjectModel
    {
        //Attributes:
        private double x;
        private double y;
        private double r;

        private HitBox hitBox;

        private double height;
        private double width;

        private double damageOnContact;

        //Constructors:
        public GameObjectModel() { }

        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * @param r
         * @param type
         * @param width
         * @param height
         * @param damageOnContact
         * 
         */
        public GameObjectModel( double x, double y, double r, double width, double height, double damageOnContact)
        {
            this.x = x;
            this.y = y;
            this.r = r;

            this.width = width;
            this.height = height;

            this.hitBox = new HitBox(this.x, this.x + this.width, this.y, this.y + this.height);

            this.damageOnContact = damageOnContact;
        }

        //Getters&setters:
        public double getX()
        {
            return x;
        }

        public void setX( double x)
        {
            this.x = x;
        }

        public double getY()
        {
            return y;
        }

        public void setY( double y)
        {
            this.y = y;
        }

        public double getR()
        {
            return r;
        }

        public void setR( double r)
        {
            this.r = r;
        }

        public HitBox getHitBox()
        {
            return this.hitBox;
        }

        public double getDamageOnContact()
        {
            return damageOnContact;
        }

        public void setDamageOnContact( double damageOnContact)
        {
            this.damageOnContact = damageOnContact;
        }

        public double getHeight()
        {
            return height;
        }

        public void setHeight( double height)
        {
            this.height = height;
        }

        public double getWidth()
        {
            return width;
        }

        public void setWidth( double width)
        {
            this.width = width;
        }

        public void setHitBox( HitBox hitBox)
        {
            this.hitBox = hitBox;
        }
    }
}
