﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class FireBallModel : DynamicGameObjectModel
    {
        //Attributes:
        private static double FIREBALL_WIDTH = 128;
        private static double FIREBALL_HEIGHT = 72;
        private static double FIREBALL_DELTA_ROTATION = 0;
        private static double FIREBALL_DELTA_X = 0;
        private static double FIREBALL_DELTA_Y = 0;
        private static double DAMAGE_ON_CONTACT = 2;

        //Constructors:

        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * @param r
         * @param type
         * @param dx
         * @param dy
         * @param damageOnContact
         * 
         */
        public FireBallModel(double x, double y, double r) : base(x, y, r, FIREBALL_WIDTH, FIREBALL_HEIGHT, FIREBALL_DELTA_X, FIREBALL_DELTA_Y, FIREBALL_DELTA_ROTATION, DAMAGE_ON_CONTACT)
        {
        
        }
    }
}
