﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class Program
    {
        static void Main(string[] args)
        {
            FireBallModel fireBall = new FireBallModel(0, 0, 0);
            FireRainModel fireRain = new FireRainModel(0, 0);
            CerberoModel cerbero = new CerberoModel(0, 0, fireBall, fireRain);

            Console.WriteLine("La prima testa di Cerbero si trova in posizione x: " + cerbero.getFirstHead().getX() + " y: " + cerbero.getFirstHead().getY());
            Console.WriteLine("La seconda testa di Cerbero si trova in posizione x: " + cerbero.getSecondHead().getX() + " y: " + cerbero.getSecondHead().getY());
            Console.WriteLine("La terza testa di Cerbero si trova in posizione x: " + cerbero.getThirdHead().getX() + " y: " + cerbero.getThirdHead().getY());
            Console.WriteLine("Il corpo di Cerbero si trova in posizione  x: " + cerbero.getCerberoBody().getX() + " y: " + cerbero.getCerberoBody().getY());

            Console.ReadLine();
        }
    }
}
