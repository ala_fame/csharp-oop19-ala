﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class HitBox
    {
        //Attributes:
        private double leftX;
        private double rightX;
        private double highY;
        private double lowY;

        //Constructor:
        /*
         * Constructor.
         * 
         * @param leftX
         * @param rightX
         * @param highY
         * @param lowY
         * 
         */
        public HitBox(double leftX, double rightX, double highY, double lowY)
        {
            this.leftX = leftX;
            this.rightX = rightX;
            this.highY = highY;
            this.lowY = lowY;
        }

        //Getters&Setters:
        public double getLeftX()
        {
            return leftX;
        }

        public void setLeftX(double leftX)
        {
            this.leftX = leftX;
        }

        public double getRightX()
        {
            return rightX;
        }

        public void setRightX(double rightX)
        {
            this.rightX = rightX;
        }

        public double getHighY()
        {
            return highY;
        }

        public void setHighY(double highY)
        {
            this.highY = highY;
        }

        public double getLowY()
        {
            return lowY;
        }

        public void setLowY(double lowY)
        {
            this.lowY = lowY;
        }

        //Methods:
        /*
         * check collisions between hitBox.
         * 
         * @param otherHitBox
         * 
         */
        public bool checkCollision(HitBox otherHitBox)
        {
            if (this.getRightX() >= otherHitBox.getLeftX() && this.getLeftX() <= otherHitBox.getRightX())
            {
                if (this.getHighY() <= otherHitBox.getLowY() && this.getLowY() >= otherHitBox.getHighY())
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * move hitBox setting specific values.
         * 
         * @param leftX
         * @param rightX
         * @param highY
         * @param lowY
         * 
         */
    public void manualHitBoxMovement(double leftX, double rightX, double highY, double lowY)
        {
            this.setLeftX(leftX);
            this.setRightX(rightX);
            this.setLowY(lowY);
            this.setHighY(highY);
        }
    }
}
