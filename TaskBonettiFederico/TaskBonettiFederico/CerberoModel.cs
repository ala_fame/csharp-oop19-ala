﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class CerberoModel
    {
        //Attributes:
        private CerberoImportantHeadModel firstHead;
        private CerberoOtherHeadModel secondHead;
        private CerberoOtherHeadModel thirdHead;
        private CerberoBodyModel cerberoBody;

        private FireBallModel fireBallModel;
        private FireRainModel fireRainModel;

        //Animation and graphic variables:
        /**
         * animation offset.
         */
        public static int FIRST_HEAD_X_OFFSET = 1200;
        /**
         * animation offset.
         */
        public static int FIRST_HEAD_Y_OFFSET = 300;
        /**
         * animation offset.
         */
        public static int MANUAL_HITBOX_X_ADJUSTEMENT = 128;
        /**
         * animation offset.
         */
        public static int MANUAL_HITBOX_Y_ADJUSTEMENT = 78;
        /**
         * animation offset.
         */
        public static int FIREBALL_X_OFFSET = -400;
        /**
         * animation offset.
         */
        public static int FIREBALL_Y_OFFSET = 400;
        /**
         * animation offset.
         */
        public static int FIRERAIN_X_OFFSET = 200;
        /**
         * animation offset.
         */
        public static int FIRERAIN_MANUAL_HITBOX_X_ADJUSTEMENT = 300;
        /**
         * animation offset.
         */
        public static int FIRERAIN_MANUAL_HITBOX_Y_ADJUSTEMENT = -400;
        /**
         * animation offset.
         */
        public static int SECOND_HEAD_ALIGN = 100;
        /**
         * animation offset.
         */
        public static int THIRD_HEAD_ALIGN = 200;
        /**
         * animation offset.
         */
        public static int CERBERO_ALIGN = 600;

        //Constructor:
        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * @param fireBallModel
         * @param fireRainModel
         * 
         */
        public CerberoModel(double x, double y, FireBallModel fireBallModel, FireRainModel fireRainModel)
        {
            firstHead = new CerberoImportantHeadModel(x, y);
            secondHead = new CerberoOtherHeadModel(x + SECOND_HEAD_ALIGN, y);
            thirdHead = new CerberoOtherHeadModel(x + THIRD_HEAD_ALIGN, y);
            cerberoBody = new CerberoBodyModel(x + CERBERO_ALIGN, y);

            this.fireBallModel = fireBallModel;
            this.fireRainModel = fireRainModel;
        }

        //Getters&Setters:
        public CerberoImportantHeadModel getFirstHead()
        {
            return firstHead;
        }

        public CerberoOtherHeadModel getSecondHead()
        {
            return secondHead;
        }

        public CerberoOtherHeadModel getThirdHead()
        {
            return thirdHead;
        }

        public CerberoBodyModel getCerberoBody()
        {
            return cerberoBody;
        }

        public FireBallModel getFireBallModel()
        {
            return fireBallModel;
        }

        public void setFireBallModel(FireBallModel fireBallModel)
        {
            this.fireBallModel = fireBallModel;
        }

        public FireRainModel getFireRainModel()
        {
            return fireRainModel;
        }

        public void setFireRainModel(FireRainModel fireRainModel)
        {
            this.fireRainModel = fireRainModel;
        }
    }
}
