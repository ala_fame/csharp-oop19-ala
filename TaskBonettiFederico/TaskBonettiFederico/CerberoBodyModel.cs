﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class CerberoBodyModel : DynamicGameObjectModel
    {
        //Attributes:
        private static double START_ROTATION = 0;
        private static double HEIGHT = 512;
        private static double WIDTH = 512;
        private static double D = 0;
        private static double DAMAGE = 0;

        //Constructor:
        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * 
         */
        public CerberoBodyModel(double x, double y) : base(x, y, START_ROTATION, HEIGHT, WIDTH, D, D, D, DAMAGE)
        {
        }
    }
}
