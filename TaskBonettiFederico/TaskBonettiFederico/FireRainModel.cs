﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBonettiFederico
{
    class FireRainModel
    {
        //Attributes:
        private FireBallModel fireBallModelA;
        private FireBallModel fireBallModelB;
        private FireBallModel fireBallModelC;

        private static double ROTATION = 270;

        //Constructor:
        /**
         * Constructor.
         * 
         * @param x
         * @param y
         * 
         */
        public FireRainModel(double x, double y)
        {
            this.fireBallModelA = new FireBallModel(x, y, ROTATION);
            this.fireBallModelB = new FireBallModel(x + CerberoModel.FIRERAIN_MANUAL_HITBOX_X_ADJUSTEMENT, y, ROTATION);
            this.fireBallModelC = new FireBallModel(x + CerberoModel.FIRERAIN_MANUAL_HITBOX_X_ADJUSTEMENT * 2, y, ROTATION);
        }

        //Getters&Setters:
        public FireBallModel getFireBallModelA()
        {
            return fireBallModelA;
        }

        public void setFireBallModelA(FireBallModel fireBallModelA)
        {
            this.fireBallModelA = fireBallModelA;
        }

        public FireBallModel getFireBallModelB()
        {
            return fireBallModelB;
        }

        public void setFireBallModelB(FireBallModel fireBallModelB)
        {
            this.fireBallModelB = fireBallModelB;
        }

        public FireBallModel getFireBallModelC()
        {
            return fireBallModelC;
        }

        public void setFireBallModelC(FireBallModel fireBallModelC)
        {
            this.fireBallModelC = fireBallModelC;
        }
    }
}
